# import the libraries
import pandas as pd
import os
import sys
import openpyxl
import snowflake.connector as snow
import numpy as np

def snowflake(f):    
    queryList = ctx.execute_string(f,remove_comments=True,return_cursors=True) 
    rows = []
    columns = []
    for col in queryList[-1].description:
        columns.append(str(col[0]))
    for query in queryList:
        for row in query:
            rows.append(row)
    df = pd.DataFrame(rows,columns=columns) 
    for x in ['Drop','Statement','Table']: 
        df = df[~df[columns[0]].str.contains(x, na=False)]
    dfFinal = df.reset_index(drop=True)
    print(dfFinal)
    return dfFinal

print("==================================================== \n Enter your Okta user name and password \n ====================================================")

snowflake_username = str(input('Okta Username: '))
snowflake_password = str(input('Okta Password: '))

# This sets up the connection
ctx = snow.connect(
            user=snowflake_username,  
            password=snowflake_password,
            authenticator='externalbrowser',
            account='pitchbook-pitchbook',
            warehouse='ANALYTICAL_WH',
            database='DATA_LAB',
            schema='PIRG',
            role='BI_READ_ONLY'
        )

# Creates the cursor objects
cur = ctx.cursor()
cur2 = ctx.cursor()

# SQL to execute
lending = "SELECT * FROM LT_LENDING_AND_MIDDLE_MARKET_LEAGUE_TABLES"
advisors = "SELECT * FROM LT_LENDING_LEAGUE_ADVISORS"

cur.execute(lending)
cur2.execute(advisors)

# These are the Pandas dataframes
df_lending = cur.fetch_pandas_all()
df_advisors = cur2.fetch_pandas_all()


# Update these with the current quarter and year you are pulling from
df_lending_current = df_lending[(df_lending['Close Quarter'] == '1Q') & (df_lending['Close Year'] == 2023)]
# advisors is for Law Firms
df_advisors_current = df_advisors[(df_advisors['Close Quarter'] == '1Q') & (df_advisors['Close Year'] == 2023)] 
# This creates the CSVs that has the data from the SQL pull
df_lending_current.to_csv('llt_data/investors.csv',index=False,encoding='cp1252')
df_advisors_current.to_csv('llt_data/advisors.csv',index=False,encoding='cp1252')


# Filters for groups and subgroups

# Geography sub-groups
geog_us = df_lending_current[(df_lending_current['Country'] == 'United States')]
geog_eur = df_lending_current[(df_lending_current['Global Region'] == 'Europe')]

# US regions
great_lakes = df_lending_current[(df_lending_current['U.S. Region'] == 'Great Lakes')]
midwest = df_lending_current[(df_lending_current['U.S. Region'] == 'Midwest')]
southeast = df_lending_current[(df_lending_current['U.S. Region'] == 'Southeast')]
mountain = df_lending_current[(df_lending_current['U.S. Region'] == 'Mountain')]
new_england = df_lending_current[(df_lending_current['U.S. Region'] == 'New England')]
south = df_lending_current[(df_lending_current['U.S. Region'] == 'South')]
mid_atlantic = df_lending_current[(df_lending_current['U.S. Region'] == 'Mid-Atlantic')]
west_coast = df_lending_current[(df_lending_current['U.S. Region'] == 'West Coast')]

# Sector
b2b = df_lending_current[(df_lending_current['Industry Sector'] == 'Business Products and Services (B2B)')]
healthcare = df_lending_current[(df_lending_current['Industry Sector'] == 'Healthcare')]
energy = df_lending_current[(df_lending_current['Industry Sector'] == 'Energy')]
financial_services = df_lending_current[(df_lending_current['Industry Sector'] == 'Financial Services')]
it = df_lending_current[(df_lending_current['Industry Sector'] == 'Information Technology')]
b2c = df_lending_current[(df_lending_current['Industry Sector'] == 'Consumer Products and Services (B2C)')]
material_resources = df_lending_current[(df_lending_current['Industry Sector'] == 'Materials and Resources')]

# Deal Type
buyouts = df_lending_current[(df_lending_current['Country'] == 'United States') & (df_lending_current['Deal Type 1'] == 'Buyout/LBO')]
secondary_buyouts = df_lending_current[(df_lending_current['Country'] == 'United States') & (df_lending_current['Secondary Buyout?'] == 'Yes')]
add_ons = df_lending_current[(df_lending_current['Country'] == 'United States') & (df_lending_current['Add-On?'] == 'Yes')]

# Deal Type select roles (FIGURE THIS OUT)
buyouts_select = df_lending_current[(df_lending_current['Country'] == 'United States') & (df_lending_current['Deal Type 1'] == 'Buyout/LBO')]
secondary_buyouts_select = df_lending_current[(df_lending_current['Country'] == 'United States') & (df_lending_current['Secondary Buyout?'] == 'Yes')]
add_ons_select = df_lending_current[(df_lending_current['Country'] == 'United States') & (df_lending_current['Add-On?'] == 'Yes')]

# Debt type
general_debt = df_lending_current[(df_lending_current['Country'] == 'United States')] 
senior_debt = df_lending_current[(df_lending_current['Country'] == 'United States') & (df_lending_current['Does the round have senior debt?'] == 1)]
revolvers = df_lending_current[(df_lending_current['Country'] == 'United States') & (df_lending_current['Does the round have revolver?'] == 1)]

# Europe regions
cee = df_lending_current[(df_lending_current['European Region'] == 'Central & Eastern Europe')]
dach = df_lending_current[(df_lending_current['European Region'] == 'GSA')]
southern_europe = df_lending_current[(df_lending_current['European Region'] == 'Southern Europe')]
france_benelux = df_lending_current[(df_lending_current['European Region'] == 'France/Benelux')]
uk_ireland = df_lending_current[(df_lending_current['European Region'] == 'UK/Ireland')]
nordics = df_lending_current[(df_lending_current['European Region'] == 'Nordic Region')]

# Law Firms (FROM ADVISORS CSV- FIGURE IT OUT)
law_firms = df_advisors_current


# Key groupings for lists

# Geography sub-groups
geog_us["List Name"] = "Most active in US"
geog_eur["List Name"] = "Most active in Europe"

# US regions
great_lakes["List Name"] = 'Most active in Great Lakes'
midwest["List Name"] = 'Most active in Midwest'
southeast["List Name"] = 'Most active in Southeast'
mountain["List Name"] = 'Most active in Mountain'
new_england["List Name"] = 'Most active in New England'
south["List Name"] = 'Most active in South'
mid_atlantic["List Name"] = 'Most active in Mid-Atlantic'
west_coast["List Name"] = 'Most active in West Coast'

# Sector
b2b["List Name"] = "B2B"
healthcare["List Name"] = "Healthcare"
energy["List Name"] = "Energy"
financial_services["List Name"] = "Financial services"
it["List Name"] = "IT"
b2c["List Name"] = "B2C"
material_resources["List Name"] = "Material resources"

# Deal Type
buyouts["List Name"] = "Most active lenders in US buyouts"
secondary_buyouts["List Name"] = "Most active lenders in US secondary buyouts, select roles"
add_ons["List Name"] = "Most active lenders in US add-ons"

# Deal type select roles
buyouts_select["List Name"] = "Most active lenders in US buyouts, select roles"
secondary_buyouts_select["List Name"] = "Most active lenders in US secondary buyouts"
add_ons_select["List Name"] = "Most active lenders in US add-ons, select roles"

# Debt type
general_debt["List Name"] = "Most active lenders to US PE companies in general debt"
senior_debt["List Name"] = "Most active lenders to US PE companies in senior debt"
revolvers["List Name"] = "Most active lenders to US PE companies in revolvers"

# Europe regions
cee["List Name"] = "Most active in Central & Eastern Europe"
dach["List Name"] = "Most active in DACH"
southern_europe["List Name"] = "Most active in Southern Europe"
france_benelux["List Name"] = "Most active in France & Benelux"
uk_ireland["List Name"] = "Most active in UK & Ireland"
nordics["List Name"] = "Most active in Nordics"

# Law firms
law_firms["List Name"] = "Most active law firms in US PE debt deals"


# Grouping dataframes together 
geog_concat = pd.concat([geog_us, geog_eur], axis=0)
us_regions_concat = pd.concat([great_lakes, midwest, southeast, mountain, new_england, south, mid_atlantic, west_coast], axis=0)
sector_concat = pd.concat([b2b, healthcare, energy, financial_services, it, b2c, material_resources
], axis=0)
deal_type_concat = pd.concat([buyouts, secondary_buyouts, add_ons], axis=0)
deal_type_select_concat = pd.concat([buyouts_select, secondary_buyouts_select, add_ons_select], axis=0)
debt_type_concat = pd.concat([general_debt, senior_debt, revolvers], axis=0)
europe_regions_concat = pd.concat([cee, dach, southern_europe, france_benelux, uk_ireland, nordics], axis=0)

# Reads in list names, groups and sub-groups
position_table_map = pd.read_csv('./table_maps/table_position_map_llt.csv')

lending_league_concat = pd.concat([geog_concat, us_regions_concat, sector_concat,deal_type_concat, deal_type_select_concat, debt_type_concat, europe_regions_concat, law_firms], axis=0)

lending_league_concat = lending_league_concat.merge(position_table_map, left_on='List Name', right_on='List', how='left')

lending_league_grouped = lending_league_concat

lending_league_grouped = lending_league_grouped[["Lender PBID", "Lender Name","Lending Flag", "List Name", "Group", "Sub-group"]]

lending_league_grouped['Counts'] = lending_league_concat.groupby(["Lender PBID", "Lender Name", "List Name"])["Lending Flag"].transform("sum")

lending_league_cleaned = lending_league_grouped.drop_duplicates()

lending_league_cleaned['Rank'] = lending_league_cleaned.groupby('List Name')['Counts'].rank(method='min', ascending=False)

lending_league_cleaned['Count of Counts'] = lending_league_cleaned.groupby(['List Name','Group','Sub-group'])['Counts'].transform('count')

lending_league_cleaned['Sum of Counts'] = lending_league_cleaned.groupby(['List Name','Group','Sub-group'])['Counts'].transform('sum')

lending_league_cleaned['All Counts Equal'] = np.where(lending_league_cleaned['Count of Counts'] == lending_league_cleaned['Sum of Counts'],1,0)

columns = ['Lender PBID', 'Rank', 'Lender Name', 'Counts', 'List Name', 'Group', 'Sub-group', 'All Counts Equal']

lending_league_cleaned = lending_league_cleaned.reindex(columns = columns)

lending_league_cleaned = lending_league_cleaned[(lending_league_cleaned['Rank']<=25) | (lending_league_cleaned['All Counts Equal'] == 1)]

lending_league_cleaned = lending_league_cleaned[(lending_league_cleaned['Counts']> 1) | (lending_league_cleaned['All Counts Equal']== 1)]

lending_league_cleaned = lending_league_cleaned[['Lender PBID', 'Rank', 'Lender Name', 'Counts', 'List Name', 'Group', 'Sub-group']]

# Renames columns so they're consistent with viz
lending_league_cleaned = lending_league_cleaned.rename(columns={'Lender PBID': "Entity PBID", 'Lender Name': 'Investor Name', 'Counts': 'Close Date'})

# CSV time
lending_league_cleaned.to_csv('llt_data/league_tables_3.csv', index=False,encoding='cp1252')